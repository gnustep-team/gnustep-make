Source: gnustep-make
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Eric Heintzmann <heintzmann.eric@free.fr>,
 Alex Myczko <tar@debian.org>,
 Yavor Doganov <yavor@gnu.org>,
Section: gnustep
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 gobjc++ <!cross>,
 gobjc++-for-host <cross>,
 pkgconf,
Build-Depends-Indep:
 texinfo <!nodoc>,
 texlive-base <!nodoc>,
 texlive-latex-base <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/gnustep-team/gnustep-make
Vcs-Git: https://salsa.debian.org/gnustep-team/gnustep-make.git
Homepage: http://gnustep.org

Package: gnustep-common
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Breaks:
 gnustep-base-common (<< 1.30.0-10),
Description: Common files for the core GNUstep environment
 This package contains the main GNUstep configuration file, common
 files, scripts and directory layout needed to run any GNUstep
 software.

Package: gnustep-multiarch
Architecture: any
Multi-Arch: same
Depends:
 gnustep-common:any (= ${binary:Version}),
 ${misc:Depends},
Provides:
 gnustep-layout-multiarch,
Description: GNUstep Multi-Arch support
 This package contains the symlinks in the GNUstep System Library
 (/usr/lib/${local:DEB-HOST-GNU-TYPE}/GNUstep) to their corresponding
 architecture-independent directories at /usr/share.

Package: gnustep-make
Architecture: all
Multi-Arch: foreign
Depends:
 autotools-dev,
 debhelper,
 gnustep-common (<< ${source:Version}.1~),
 gnustep-common (>= ${source:Version}),
 gobjc++ | gobjc++-for-host,
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 gnustep-make-doc,
Provides:
 dh-gnustep,
Description: GNUstep build system
 This package contains the makefiles needed to compile any GNUstep
 software, and the GNUstep Test Framework used by GNUstep packages to
 build and run tests.

Package: gnustep-make-doc
Architecture: all
Build-Profiles: <!nodoc>
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Description: Documentation for GNUstep Make
 This package contains the GNUstep Make manual in Info, HTML and PDF
 format.  Also included are several documents such as the GNUstep
 HOWTO, FAQ and explanation of the GNUstep filesystem which are useful
 to get familiar with the basic aspects of the GNUstep environment.
